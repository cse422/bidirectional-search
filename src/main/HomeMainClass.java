package main;

import nodes.Vertex;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class HomeMainClass {
    public static void main(String[] args) throws Exception {
        Engine engine = new Engine();
        engine.setGraph();
        //map.print();
        engine.Bidirectional_Search();
    }
}
